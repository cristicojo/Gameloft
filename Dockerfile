FROM openjdk:11
ADD target/gameloft.jar gameloft.jar
EXPOSE 8080
ENTRYPOINT ["java","-Dspring.data.mongodb.uri=mongodb://mongo:27017/local", "-jar", "gameloft.jar"]
